#docker build -t smart-job:1.0 .
#docker run -d -p 8080:8080 --name smart-job smart-job:1.0

FROM gradle:jdk17 AS build
WORKDIR /home/gradle/src
COPY . .
RUN ./gradlew bootJar --no-daemon

FROM openjdk:17-jdk-slim
ENV SPRING_PROFILES_ACTIVE=dev
EXPOSE 8080
COPY --from=build /home/gradle/src/build/libs/smart-job-0.0.1.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
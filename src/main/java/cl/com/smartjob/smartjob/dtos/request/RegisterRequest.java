/* (C) 2024 agalvis */
package cl.com.smartjob.smartjob.dtos.request;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import java.util.List;

public record RegisterRequest(
        @NotNull String name,
        @NotNull
                @Pattern(
                        regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$",
                        message = "Email not valid")
                @NotNull
                String email,
        @NotNull String password,
        List<PhonesRequest> phones) {

    @Override
    public String toString() {
        return "RegisterRequest{"
                + "name='"
                + name
                + '\''
                + ", email='"
                + email
                + '\''
                + ", password='"
                + password
                + '\''
                + ", phones="
                + phones
                + '}';
    }
}

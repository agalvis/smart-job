/* (C) 2024 agalvis */
package cl.com.smartjob.smartjob.controllers;

import cl.com.smartjob.smartjob.dtos.request.RegisterRequest;
import cl.com.smartjob.smartjob.models.User;
import cl.com.smartjob.smartjob.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

    private final UserService service;

    @GetMapping
    @Operation(summary = "Get all users", description = "Get all users from database", parameters = {
            @Parameter(in = ParameterIn.HEADER, name = "Authorization", required = true)
    })
    @SecurityRequirement(name = "Bearer Authentication")
    public List<User> list() {
        return service.findAll();
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(request));
    }
}

/* (C) 2024 agalvis */
package cl.com.smartjob.smartjob.dtos.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public record PhonesRequest(
        String number,
        @JsonProperty("citycode") String cityCode,
        @JsonProperty("contrycode") String countryCode) {}

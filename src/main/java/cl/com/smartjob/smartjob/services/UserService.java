/* (C) 2024 agalvis */
package cl.com.smartjob.smartjob.services;

import cl.com.smartjob.smartjob.dtos.request.RegisterRequest;
import cl.com.smartjob.smartjob.models.Phone;
import cl.com.smartjob.smartjob.models.User;
import cl.com.smartjob.smartjob.models.repositories.UserRepository;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    private final UserRepository repository;

    private final PasswordEncoder passwordEncoder;

    private final String regex;

    public UserService(
            UserRepository repository,
            PasswordEncoder passwordEncoder,
            @Value("${password-regex}") String regex) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
        this.regex = regex;
    }

    @Transactional(readOnly = true)
    public List<User> findAll() {
        return (List<User>) repository.findAll();
    }

    @Transactional
    public User save(RegisterRequest request) {
        validatePassword(request.password());
        List<Phone> phones =
                request.phones().stream()
                        .map(
                                phone ->
                                        Phone.builder()
                                                .number(phone.number())
                                                .cityCode(phone.cityCode())
                                                .countryCode(phone.countryCode())
                                                .build())
                        .toList();
        User user =
                User.builder()
                        .name(request.name())
                        .email(request.email())
                        .password(passwordEncoder.encode(request.password()))
                        .phones(phones)
                        .build();
        return repository.save(user);
    }

    private void validatePassword(String password) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(
                    "Password must have at least 8 characters,"
                            + "one uppercase, one lowercase,"
                            + "one number and one special character");
        }
    }
}

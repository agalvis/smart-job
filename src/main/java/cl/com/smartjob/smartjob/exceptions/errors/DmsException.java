/* (C) 2024 agalvis */
package cl.com.smartjob.smartjob.exceptions.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DmsException extends RuntimeException {
    public DmsException(String message) {
        super(message);
    }
}

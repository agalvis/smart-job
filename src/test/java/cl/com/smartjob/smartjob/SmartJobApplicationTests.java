/* (C) 2024 agalvis */
package cl.com.smartjob.smartjob;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SmartJobApplicationTests {

    @Test
    void contextLoads() {}
}

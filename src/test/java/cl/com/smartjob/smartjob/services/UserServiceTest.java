/* (C) 2024 agalvis */
package cl.com.smartjob.smartjob.services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

import cl.com.smartjob.smartjob.dtos.request.RegisterRequest;
import cl.com.smartjob.smartjob.models.User;
import cl.com.smartjob.smartjob.models.repositories.UserRepository;
import java.util.ArrayList;
import java.util.List;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.aot.DisabledInAotMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {UserService.class, String.class, PasswordEncoder.class})
@ExtendWith(SpringExtension.class)
@DisabledInAotMode
class UserServiceTest {
    private static final EasyRandom generator = new EasyRandom();
    private static final User user = generator.nextObject(User.class);
    @MockBean private PasswordEncoder passwordEncoder;

    @MockBean private UserRepository repository;

    @Autowired private UserService service;

    private static final String regex =
            "^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[@#$%^&*!])[A-Za-z\\d@#$%^&*!]{8,}$";

    /** Method under test: {@link UserService#findAll()} */
    @Test
    void testFindAll() {
        // Arrange
        ArrayList<User> userList = new ArrayList<>();
        when(repository.findAll()).thenReturn(userList);

        // Act
        List<User> actualFindAllResult = service.findAll();

        // Assert
        verify(repository).findAll();
        assertTrue(actualFindAllResult.isEmpty());
        assertSame(userList, actualFindAllResult);
    }

    /** Method under test: {@link UserService#save(RegisterRequest)} */
    @Test
    void testSave() {
        // Arrange
        when(repository.save(any())).thenReturn(user);
        UserService userService = new UserService(repository, new BCryptPasswordEncoder(), regex);
        // Act
        User actualSaveResult =
                userService.save(
                        new RegisterRequest(
                                "Name", "jane.doe@example.org", "Password123!", new ArrayList<>()));
        // Assert
        verify(repository).save(isA(User.class));
        assertSame(user, actualSaveResult);
    }

    /** Method under test: {@link UserService#save(RegisterRequest)} */
    @Test
    void testSaveErrorPassword() {
        // Arrange
        UserRepository repository = mock(UserRepository.class);
        UserService userService = new UserService(repository, new BCryptPasswordEncoder(), regex);

        // Act and Assert
        assertThrows(
                IllegalArgumentException.class,
                () ->
                        userService.save(
                                new RegisterRequest(
                                        "Name",
                                        "jane.doe@example.org",
                                        "iloveyou",
                                        new ArrayList<>())));
    }

    /** Method under test: {@link UserService#save(RegisterRequest)} */
    @Test
    void testSaveErrorEmailAlreadyExists() {
        // Arrange
        when(repository.save(any())).thenThrow(new DataIntegrityViolationException("x"));
        UserService userService = new UserService(repository, new BCryptPasswordEncoder(), regex);
        // Act and Assert
        assertThrows(
                DataIntegrityViolationException.class,
                () ->
                        userService.save(
                                new RegisterRequest(
                                        "Name",
                                        "jane.doe@example.org",
                                        "Password123!",
                                        new ArrayList<>())));
        verify(repository).save(isA(User.class));
    }
}

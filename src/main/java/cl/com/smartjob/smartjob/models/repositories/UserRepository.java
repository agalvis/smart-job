/* (C) 2024 agalvis */
package cl.com.smartjob.smartjob.models.repositories;

import cl.com.smartjob.smartjob.models.User;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, UUID> {
    boolean existsByEmail(String email);

    Optional<User> findByEmail(String email);
}

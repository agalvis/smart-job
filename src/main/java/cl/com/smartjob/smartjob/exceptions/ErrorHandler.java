/* (C) 2024 agalvis */
package cl.com.smartjob.smartjob.exceptions;

import cl.com.smartjob.smartjob.exceptions.errors.NotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import java.net.ConnectException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

@ControllerAdvice
public class ErrorHandler {

    public static final String ERROR = "error";
    public static final String TIMESTAMP = "timestamp";

    public ErrorHandler() {}

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<?> MethodArgumentNotValidException(
            MethodArgumentNotValidException ex, HttpServletRequest request) {
        List<FieldError> fieldErrors = ex.getFieldErrors();
        Map<String, Object> messages =
                fieldErrors.stream()
                        .collect(
                                Collectors.toMap(
                                        FieldError::getField,
                                        FieldError::getDefaultMessage,
                                        (existing, replacement) -> existing,
                                        LinkedHashMap::new));
        ErrorResponse errorResponse =
                buildErrorResponse(ex.getStatusCode().value(), messages.toString(), request);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({IllegalStateException.class})
    public ResponseEntity<?> handleIllegalStateException(
            IllegalStateException e, HttpServletRequest request) {
        ErrorResponse errorResponse =
                buildErrorResponse(HttpStatus.BAD_REQUEST.value(), e.getMessage(), request);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({HttpClientErrorException.class})
    public ResponseEntity<?> handleHttpClientException(
            HttpClientErrorException e, HttpServletRequest request) {
        ErrorResponse errorResponse =
                buildErrorResponse(
                        e.getStatusCode().value(),
                        HttpStatus.valueOf(e.getStatusCode().value()).getReasonPhrase(),
                        request);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({HttpServerErrorException.class})
    public ResponseEntity<?> handleHttpServerException(
            HttpServerErrorException e, HttpServletRequest request) {
        ErrorResponse errorResponse =
                buildErrorResponse(
                        e.getStatusCode().value(),
                        HttpStatus.valueOf(e.getStatusCode().value()).getReasonPhrase(),
                        request);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> handleNotFoundException(
            NotFoundException ex, HttpServletRequest request) {
        ErrorResponse errorResponse =
                buildErrorResponse(
                        HttpStatus.NOT_FOUND.value(),
                        HttpStatus.NOT_FOUND.getReasonPhrase(),
                        request);
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exception(Exception e, HttpServletRequest request) {
        ErrorResponse errorResponse =
                buildErrorResponse(
                        HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), request);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<?> constraintViolationException(
            DataIntegrityViolationException e, HttpServletRequest request) {
        ErrorResponse errorResponse =
                buildErrorResponse(
                        HttpStatus.INTERNAL_SERVER_ERROR.value(), "Email already in use", request);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ResourceAccessException.class)
    public ResponseEntity<?> resourceAccessException(
            ResourceAccessException e, HttpServletRequest request) {
        ErrorResponse errorResponse =
                buildErrorResponse(HttpStatus.REQUEST_TIMEOUT.value(), e.getMessage(), request);
        return new ResponseEntity<>(errorResponse, HttpStatus.REQUEST_TIMEOUT);
    }

    @ExceptionHandler(ConnectException.class)
    public ResponseEntity<?> connectException(ConnectException e, HttpServletRequest request) {
        ErrorResponse errorResponse =
                buildErrorResponse(HttpStatus.REQUEST_TIMEOUT.value(), e.getMessage(), request);
        return new ResponseEntity<>(errorResponse, HttpStatus.REQUEST_TIMEOUT);
    }

    private static ErrorResponse buildErrorResponse(
            int errorCode, String message, HttpServletRequest request) {
        return ErrorResponse.builder().message(message).build();
    }
}
